# gpt\_with\_tools

This is a simple Python program demonstrating how
OpenAI's GPT can learn to use other programs as
computational tools.  This program teaches GPT-4
to use a calculator to evaluate arithmetical expressions and to
use Semantic Scholar to verify citations to scientific papers. 
It can be easily modified to teach GPT to use other tools, such as accessing a private database or searching the web in real time.

The programs's approach is to teach GPT to output
a command to the user when it wants to use an external tool.  The program intercepts the command, runs the arguments to the command through a corresponding subroutine, and types the result
returned by the subroutine to GPT.  GPT then incorporates the information into its next response.

## Installing

Clone from [GitLab](https://gitlab.com/HenryKautz/gpt_with_tools).  

Install required packages:

    pip install openai plusminus

Obtain an OpenAI API key from [https://openai.com/blog/openai-api](https://openai.com/blog/openai-api).  If you do not already have a developer
account, you will need to subscribe or apply for free
research API credits.  (Note that the free credits expire after a few months whether or not you use them.)

You will need both an API Key and an Organization Identifier.  You can find your API key by clicking on your user icon at the top right of the screen and then selecting "View API Key".  You only get one chance to see your full API Key, so copy it down.  If you forget what it is, you can create another API key on this page.

Assuming you have an individual account, on the API key page you will see that your default organization is listed as "personal".

Now click on Settings on the left-side bar. Here you will see your organization name again ("personal") and an Organization ID number.  Copy down the ID number.

In your .profile or .bashrc file (or similar for other operating systems), set shell variables to the values you copied down:

    export MY_GPT_KEY="Some Value"
    export MY_GPT_ORG="Some Other Value"

Create a new terminal window so that these shell variables are available.

## Installing the SAT solver tools

The SAT solver requires installation of 
<a href="https://gitlab.com/HenryKautz/Schema">Schema</a>, <a href="https://www.sbcl.org/">Common Lisp</a>, and the SAT solver <a href="https://github.com/arminbiere/kissat">kissat</a>.  

You will need 
to edit the path for Schema in the file satsolver.sh, and then
copy satsolver.sh to your bin directory.

## Running the program

In the terminal execute

    python gpt_with_tools.py {OPTIONS}

The options are:

* --debug or -d: Print the prompt that teaches GPT how to use the tools, and print the information 
that is passing between GPT and the appropriate tool each
time one is invoked.

To end the read-eval loop, type "exit"
at the user prompt.  During the loop, you can enter a line beginning with +d to turn
on debugging output or -d to turn off debugging output.

## Understanding the code

Calls to the GPT API are stateless, so every call contains the history of what the user has said
and what GPT has said, as saved in the list "messages".  Messages is initialized with instructions from
the user to GPT about how to use each tool, including example exchanges between the user
and GPT (called the "assistant" in the code) demonstrating what GPT should do.  Note that the example
exchanges do not have to have actually occurred.  The examples are critical for helping GPT understand
the instructions correctly.

In addition to the "user" and "assistant" entries in messages, one can include an initial entry labeled "system". 
The code uses the default ChatGPT entry for "system".  People on the ChatGPT forums have not determined how to 
use the system entry effectively.

The interface and initialization messages for each tool is defined by a class that is a subclass of Tool and appears in a file named after the tool.  In order to add a new tool, define the new tool subclass and update the following lines in gpt\_with\_tools.py:

	# Import tool classes
	from Calculator import Calculator
	from CitationChecker import CitationChecker
	Tool_Classes = [ Calculator, CitationChecker ]
	
Each tool class includes the following fields and methods:

* `pattern`: a regular expression that matches outputs from GPT that invoke the tool.
* `name`: a string that identifies the tool.
* `debug`: if set to true, the tool should print debugging information when it runs.
* `run(self,m:re.Match)->str`: the method that is passed the match object from the pattern. It runs the tool and returns a string that is passed back to GPT.
* `instructions`: a series of user and assistant messages that teach GPT to use the tool.

If you want to modify the code, you should start by using the regular OpenAI interface to teach GPT to issue
a command you create when it needs to perform some task that is beyond its capabilities, and manually simulate
returning the results of the task to GPT.

## Example

GPT often makes mistakes in even simple arithmetic when numbers become large.  However, it is
quite good at symbolic mathematics.  This example that now appears in the initialization messages was
actually carried out by GPT with an early version of this program.

```
User: Please calculate the definite integral of sin(x)*cos(x) from x=0.2 to x=0.8.

Assistant: To calculate the definite integral of sin(x)*cos(x) from x=0.2 to x=0.8, we can use the following antiderivative:

The antiderivative of sin(x)*cos(x) is 0.5 * sin^2(x) + C, where C is the constant of integration.

Now we can evaluate the definite integral:

∫(sin(x)*cos(x))dx from 0.2 to 0.8 = (0.5 * sin^2(0.8) - 0.5 * sin^2(0.2))

Let me calculate the values:

CALCULATE(0.5 * sin(0.8)^2 - 0.5 * sin(0.2)^2)

[Program evaluates the expression and returns it to GPT]

Assistant: The definite integral of sin(x)*cos(x) from x=0.2 to x=0.8 is approximately 0.2376.
```


## Contact

For suggestions, improvements, or other feedback, please contact [Henry Kautz](https://henrykautz.com) at [henry.kautz@gmail.com](mailto:henrykautz@gmail.com).