from Tool import Tool, msg_list
import re
import plusminus

class Calculator(Tool):
    calculator = plusminus.ArithmeticParser()
    
    name='Calculator'
    pattern = r'(?m)^[ \t]*CALCULATE\((.+)\)[ \t]*$'

    def run(self,match:re.Match)->str:
        expression = match.group(1)
        value=None
        try:
            value = str(self.calculator.evaluate(expression))
            return value
        except:
            print(f"Calculator error!")
            print(f"  Input expression is {expression}")
            print(f"  Returned value is {value}")
            return('CALCULATE failed, please try a different way to evaluate the expression')
    
    instructions = [
        {'role':'user', 'content':
            'In order to help you do arithmetic, I am giving you the ability '
            'to use a calculator.  All you need to do when you need to evaluate '
            'an arithmetic expression is for you to output CALCULATE(<expression>), where <expression> is a '
            'arithmetic expression.  After you output CALCULATE(<expression>) for some '
            '<expression> you should stop writing immediately and wait. '
            'It will then appear to you that I am typing in the result of '
            'evaluating the expression.  An <expression> can be made up numbers,  the arithmetic '
            'operators +, *, / (division), and ** (exponentiation), the functions sin, cos, tan, '
            'arcsin, arccos, arctan, ln (natural logarithm), and sqrt (square root), and '
            'subexpressions in parentheses. '
            'Remember especially that exponentiation is written as ** and not as ^. '
            'The calculator does  not know any other functions. '
            'When you call CALCULATE, the CALCULATE(<expression>) statement should appear on a '
            'line by itself with no other text before or after it. '
            'You stop writing your answer as soon as you finish the CALCULATE(<expression>) '
            'statement. I will then type a number that is the result of evaluating the '
            'expression. You should then repeat the number I just typed. '
            'If you are not done solving the original '
            'problem you were given, then you should us the number to continue '
            'solving the problem. '
            'Do you understand?'
        },
        {'role':'assistant', 'content':
            'Yes, I understand.  Whenever I need to evaluate an arithmetic expression, I can '
            'output CALCULATE(<expression>) and then wait for a number from you. '
            'I should echo the number back out and then use the number to solve the original '
            'problem I was given.'
        },
        {'role':'user', 'content':
            'How much is 42342343 times 3234.34?'
        },
        {'role':'assistant', 'content':
            'CALCULATE(42342343 * 3234.34)'
        },
        {'role':'user', 'content':
            '136949533658.62001'
        },
        {'role':'assistant', 'content':
            '42342343 times 3234.34 is 136949533658.62001.'
        },
        {'role':'user', 'content':
            'What costs less, 150 pounds of apples at $1.35 per pound, or 575 bananas at $0.35 each?'
        },
        {'role':'assistant', 'content':
            'We first determine the cost of the apples by multiplying the weight by the price per pound.\n'
            'CALCULATE(150 * 1.35) '
            },
        {'role':'user', 'content':
            '202.5'
        },
        {'role':'assistant', 'content':
            'The cost of the the apples is $202.50.\n'
            'We next determine the cost of the banana by multiplying the number of bananas by the price each.\n'
            'CALCULATE(250 * 0.35)'
        },
        {'role':'user', 'content':
            '201.25'
        },
        {'role':'assistant', 'content':
            'The cost of the bananas is $201.25.\n'
            'The cost of the bananas is less than the cost of the apples, so the answer is the bananas.'
        },
        {'role':'user', 'content':
            'Please calculate the definite integral of sin(x)*cos(x) from x=0.2 to x=0.8. '
            'You may use the CALCULATE API to help, but remember that CALCULATE itself does '
            'not know how to integrate.'
        },
        {'role':'assistant', 'content':
            'To calculate the definite integral of sin(x)*cos(x) from x=0.2 to x=0.8, we can use the following antiderivative:\n\n'
            'The antiderivative of sin(x)*cos(x) is 0.5 * sin^2(x) + C, where C is the constant of integration.\n\n'
            'Now we can evaluate the definite integral: \n\n'
            '∫(sin(x)*cos(x))dx from 0.2 to 0.8 = (0.5 * sin^2(0.8) - 0.5 * sin^2(0.2))\n\n'
            'Let me calculate the values:\n\n'
            'CALCULATE(0.5 * sin(0.8)^2 - 0.5 * sin(0.2)^2)'
        },
        {'role':'user', 'content':
            '0.2375651290760435'
        },
        {'role':'assistant', 'content':
            'The definite integral of sin(x)*cos(x) from x=0.2 to x=0.8 is approximately 0.2376'
        },
    ]