from Tool import Tool, msg_list
from gpt_with_tools import gpt_with_tools_dir
import re
import subprocess


class SatSolver(Tool):
    
    name='SatSolver'
    pattern = r'(?m)SOLVE\[(.+)\]'

    def run(self,match:re.Match)->str:
        expression = match.group(1)
        value=None
        try:
            # Call to SCHEMA and a SAT solver here
            with open("gpt.wff", "w") as schemaFile:
                schemaFile.write(expression)
            # Run solver
            # Get result from file solver creates
            sp = subprocess.run(f"{gpt_with_tools_dir}/satsolver.sh", "gpt", capture_output=True, text=True)
            return sp.stdout

        except:
            print(f"SatSolver error!")
            print(f"  Input expression is {expression}")
            print(f"  Returned value is {value}")
            return('SatSolver failed')
    
    instructions = [
         {'role':'user', 'content':
            'In order to help you reason logically, I am giving you the ability '
            'to use a satisfiability solver for propositional logic. '
            'When you want to find a satisfying assigning to logical formal, you '
            'should output SOLVE[<formula>], where <formula> is a formula in prenex '
            'form Boolean logic using LISP notation. '
            'The formula may span multiple lines, and can include comments '
            'using lines that begin with a semi-colon ";". '
            'The next input you read will be the resulting of running a satisfiability '
            'solver on the formula.  If the formula is unsatisfiable, this will be '
            'the UNSAT[].  If the formula is satisfiable, this will be in the '
            'form SAT[<propositions>] where <propositions is a series of ground propositions '
            'separated by whitespace which represent the propositions that are true in '
            'some satisfying assignment. '
        },

        {'role':'assistant', 'content':
            'Okay, I understand.'
        },

        {'role':'user', 'content':
            'Here is an example.  Suppose I write '
            '\n'
            'John and Mary each own a house. '
            'One house is red and the other is blue. '
            'Each has a different pet that is either a cat or a dog. '
            'The cat lives in the blue house. '
            "Mary does not own a dog. "
            'Who owns which color house and which pet?'
	        '\n'
	        'The puzzle is a kind of constraint-satisfaction problem called a “Zebra Puzzle”.  In a Zebra Puzzle, '
            'you have to the determine the properties of a set of entities.  Each entity has '
            'different unique values for the properties.  It is an implicit constraint in Zebra '
            'Puzzle that \n'
            '1. Each entry has a value for each property.  '
            'For example, the house at position 1 has a color.\n'
            '2. Each entity has no more than one value for each property. '
            'For example, if the house at position 1 is yellow, it is not blue.\n'
            '3. No two entities have the same value for a given property.  For example, '
            'if the house at position 1 is yellow, then the house '
            'at position 2 is not yellow.\n'
            'We call the set of all of these contraints the uniqueness constraints '
            'for the puzzle. '
            'Remember to be consistent about what are entities and what are properites. '
            'Each proposition in your encoding relates a entity to a property. '
            'For example, a proposition in this puzzle might be that the house at position 1 has '
            'the value yellow for its color property.  In the encoding, there should not be '
	        'any propositions that relate two properties, such as pet and color.\n'
	        'Not every logic problem is a Zebra problem.  Try to first decide whether or not the '
	        'problem I give you is a Zebra problem.  If you are not sure, then ask me.\n'
        },
        
        {'role':'assistant', 'content':
            'I will represent this Zebra puzzle in logic and call a satisfiabilitty solver '
            'using the following propositions:\n'
            "JohnRed: John's house is red \n"
            "JohnBlue: John's house is blue \n"
            "MaryRed: Mary's house is red \n"
            "MaryBlue: Mary's house is blue \n" 
            "JohnCat: John owns the cat \n"
            "JohnDog: John owns the dog \n"
            "MaryCat: Mary owns the cat \n"
            "MaryDog: Mary owns the dog \n"            
            'SOLVE[ \n'
                '(AND \n'
                    "; One house is red and other is blue \n"
                    '(or JohnRed JohnBlue) \n'
                    '(or MaryRed MaryBlue) \n'
                    '(not (and JohnRed JohnBlue)) \n'
                    '(not (and MaryRed MaryBlue)) \n'
                    '(not (and JohnRed MaryRed)) \n'
                    '(not (and JohnBlue MaryBlue)) \n'
                    "; Each of John and Mary has a different pet that is either a cat or a dog \n"
                    '(or JohnCat JohnDog) \n'
                    '(or MaryCat MaryDog) \n'
                    '(not (and JohnCat JohnDog)) \n'
                    '(not (and MaryCat MaryDog)) \n'
                    '(not (and JohnCat MaryCat)) \n'
                    '(not (and JohnDog MaryDog)) \n'  
                    "; The cat lives in the blue house. \n"  
                    '(or (and JohnBlue JohnCat) (and MaryBlue MaryCat) \n'
                    "; Mary does not own a dog. \n"
                    '(not MaryDog) \n'
                ')'
        },

        {'role':'user', 'content':
            'SAT[\n'
            'OWNERAISALICE\n'
            'OWNERBISCHARLES\n'
            'OWNERCISBOB\n'
            'PETAISCAT\n'
            'PETBISBIRD\n'
            'PETCISDOG\n'
            ']'
        },

        {'role':'assistant', 'content':
            'The owner of house A is Alice, house B is Charles, and house C is Bob.  The cat lives '
            'in house A, the bird lives in house B, and the dog lives in house C.'
        },

        {'role':'user', 'content':
            'A different kind of logic problem is to determine if a conclusion logically follows '
            'from a set of assumptions.  The way to encode the problem is to encode the assumptions as '
            'before, and then add the negation of the conclusion.  If the resulting formula is unsatisfiable, '
            'then it must be the case that the conclusion does follow from the assumptions, because there is '
            'no way to satisfy the assumption while making the conclusion false.  We call this kind of '
            'problem a deduction problem.  Consider the following problem:\n'
            'Jack is looking at Anne. Anne is looking at George. Jack is married, George is '
            'not, and we don’t know if Anne is married. Is a married person looking at an '
            'unmarried person?\n'
            'Here is how this problemis a deduction problem.  The assumptions are:\n'
            '1. Jack is looking at Anne. \n'
            '2. Anne is looking at George. \n'
            '3. Jack is married. \n'
            '4. George is not married \n'
            '5. we don’t know if Anne is married \n'
            'The conclusion we want to test is: Is a married person looking at an unmarried person? '
            'So the negation of the encoding of this conclusion should be added to the formula. '
            'Please encode the problem and call the sat solver.'
        },
        {'role':'assistant', 'content':
            'SOLVE[ \n'
            '(AND \n'
            '  ; Assumptions \n'
            '  JackLookingAtAnne \n'
            '  AnneLookingAtGeorge \n'
            '  JackMarried \n'
            '  (NOT GeorgeMarried) \n'
            '  (OR AnneMarried (NOT AnneMarried)) \n'
            '  ; Negation of the conclusion \n'
            '  (NOT (OR \n'
            '    (AND JackLookingAtAnne (AND JackMarried (NOT AnneMarried))) \n'
            '    (AND AnneLookingAtGeorge (AND AnneMarried (NOT GeorgeMarried))))\n'
            '  )\n'
            ' )\n'
            '] \n'
        },
        {'role':'user', 'content':
            'UNSAT[]'
        },
        {'role':'assistant', 'content':
            'The result from the satisfiability solver is UNSAT[], indicating that the formula '
            'is unsatisfiable. This means that the assumptions combined with the negation of the '
            'conclusion cannot all be true simultaneously. Therefore, the conclusion logically '
            'follows from the assumptions.'
        },
        {'role':'user', 'content':
            'You now know how to use the satisfiability solver to solver constraint satisfaction '
            'problems, which include Zebra puzzles as well as other kinds of problems where the '
            'task is to find a truth assignment that satisfies a conjunction of constraints. '
            'You also know how to solve a deduction problem where the task is to determine '
            'if a conclusion follows from a set of assumptions. '
            'When you need to solve a logical reasoning problem, you should start by '
            'determining if it is a constraint satisfaction problem or a deduction problem, '
            'and proceed accordingly.'
        },
        {'role':'assistant', 'content':
            'Okay, I will do that.'
        }	
    ]